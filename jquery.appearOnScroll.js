(function ($){

    $(window).on('scroll', function(e){

        var scroll_top = $(window).scrollTop();

        $('.appear-on-scroll').each(function(){
            var self = $(this);

            var scroll_limit = self.data('scroll-limit');
            var scroll_start = self.data('scroll-start');
            var scroll_speed = self.data('scroll-speed');
            var scroll_origin = self.data('scroll-origin');


            console.log(scroll_top, scroll_start);

//            self.offset({top: self.offset().top});
            if(scroll_top < scroll_start){
                self.offset({top: scroll_origin});
                return;
            }

            var new_top = scroll_origin+(scroll_top-scroll_start)*scroll_speed;
            console.log('new:', new_top);

            var limit = (scroll_origin + scroll_limit);
            console.log('limit', limit);

            if(new_top < limit){
                console.log('GO');
                self.offset({top: new_top});
            } else{
                console.log('DONE');
                self.offset({top: limit});
            }

        });

    });

    $.fn.appearOnScroll = function(options){
        options = options || {};

        return this.each(function(){
            var $this = $(this);

            // starting position of element
            options.origin = options.origin || -$this.outerHeight();
             // at which window.scrollTop should the scrolling begin
            options.start = options.start || 0;
            // how many pixels of element should scroll in
            options.limit = options.limit || $this.outerHeight();
            // how fast should the scrolling be
            options.speed = options.speed || 0.3;
            options.zindex = options.zindex || 100;

            $this.addClass('appear-on-scroll');
            $this.css('position', 'fixed');
            $this.offset({top: options.origin});
            $this.css('z-index', options.zindex);

            $this.data('scroll-origin', options.origin);
            $this.data('scroll-start', options.start);
            $this.data('scroll-limit', options.limit);
            $this.data('scroll-speed', options.speed);
        });
    };


})( jQuery );
