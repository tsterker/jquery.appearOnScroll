jquery.appearOnScroll
=========================

make elements appear on the scroll event. some options can be supplied

Example
=========================


> index.html
```html
<style>div{width: 100%; padding: 1em; background-color: gray;}</style>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.min.js"></script>
<script src="jquery.appearOnScroll.js"></script>
<h1>Some Header</h1>
<div class="apply-to-me">I will appear at some point</div>
<div>
  ...scroll...<br><br><br><br><br><br><br><br><br><br><br><br>
  <div id="trigger">START APPEARING HERE</div>
  ...scroll...<br><br><br><br><br><br><br><br><br><br><br><br>
  ...scroll...<br><br><br><br><br><br><br><br><br><br><br><br>
</div>
<script>
$('.apply-to-me').appearOnScroll({start: $('#trigger').offset().top, speed: 0.1});
</script>
```
